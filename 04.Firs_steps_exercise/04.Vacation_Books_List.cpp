#include "iostream"
using namespace std;
int main() {
    int numberOfPagesInBook, pagesPerHour, days, timePerBook;
    cin >> numberOfPagesInBook >> pagesPerHour >> days;
    timePerBook = (numberOfPagesInBook - (numberOfPagesInBook % pagesPerHour)) / pagesPerHour;
    cout << timePerBook / days << endl;
    return 0;
}