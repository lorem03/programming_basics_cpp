#include "iostream"
#include "math.h"
#include <cmath>
using namespace std;
int main() {
    double rad;
    double deg;
    cin >> rad;
    deg = rad * 180 / 3.14;
    cout << round(deg) << endl;
    return 0;
}