#include "iostream"
using namespace std;
int main() {
    double strawbery, strawberyPrice, bananas, bananasPrice, oranges, orangesPrice, raspberries, raspberriesPrice, total;
    cin >> strawberyPrice >> bananas >> oranges >> raspberries >> strawbery;
    raspberriesPrice = strawberyPrice / 2;
    orangesPrice = raspberriesPrice * 0.6;
    bananasPrice = raspberriesPrice * 0.2;
    cout.setf(ios::fixed);
    cout.precision(2);
    total = strawbery * strawberyPrice + bananas * bananasPrice + oranges * orangesPrice + raspberries * raspberriesPrice;
    cout << total << endl;
    return 0;
}