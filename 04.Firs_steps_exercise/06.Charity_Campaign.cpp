#include "iostream"
using namespace std;
int main() {
    int days, bakers, cakes, waffles, pancakes;
    double cakePrice, wafflesPrice, pancakesPrice, bakersPerDayPrice, income, total;
    cakePrice = 45.0;
    wafflesPrice = 5.8;
    pancakesPrice = 3.2;
    cin >> days >> bakers >> cakes >> waffles >> pancakes;
    bakersPerDayPrice = (cakes * cakePrice + waffles * wafflesPrice + pancakes * pancakesPrice) * bakers;
    income = bakersPerDayPrice * days;
    total = income - (income / 8);
    cout.setf(ios::fixed);
    cout.precision(2);
    cout << total << endl;
    return 0;
}