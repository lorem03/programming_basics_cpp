#include "iostream"
using namespace std;
int main() {
    double depositAmount, nonConvertedPercent, percentPerYear, percentPerMonth, finalAmount;
    int months;
    cin >> depositAmount >> months >> nonConvertedPercent;
    percentPerYear = depositAmount * (nonConvertedPercent / 100);
    percentPerMonth = percentPerYear / 12;
    finalAmount = depositAmount + months * percentPerMonth;
    cout.setf(ios::fixed);
    cout.precision(2);
    cout << finalAmount << endl;
    return 0;
}