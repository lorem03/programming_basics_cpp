#include "iostream"
using namespace std;
int main() {
    int lenght, width, height;
    double percentage, convertedPercent;
    cin >> lenght >> width >> height >> percentage;
    convertedPercent = percentage * 0.01;
    cout.setf(ios::fixed);
    cout.precision(2);
    cout << ((lenght * width * height) * 0.001) * (1 - convertedPercent) << endl;
    return 0;
}