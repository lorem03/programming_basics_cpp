#include "iostream"
using namespace std;
int main() {
    int rentPrice;
    double cakePrice, softDrinks, animator;
    cin >> rentPrice;
    cakePrice = (rentPrice * 1.00) / 5;
    softDrinks = cakePrice * 0.55;
    animator = (rentPrice * 1.00) / 3;
    cout << rentPrice + cakePrice + softDrinks + animator << endl;
    return 0;
}