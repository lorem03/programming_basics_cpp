#include <iostream>
using namespace std;
int main() {
    int n, nums, max = INT32_MIN;
    cin >> n;
    while (n > 0) {
        cin >> nums;
        n -= 1;
        if (nums > max) {
            max = nums;
        }
    }
    cout << max << endl;
    return 0;
}