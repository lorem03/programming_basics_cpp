#include <iostream>
using namespace std;
int main() {
    cout.setf(ios::fixed);
    cout.precision(2);
    
    int topupCount, input = 0;
    cin >> topupCount;
    double amount = 0, money = 0;
    while (input < topupCount) {
        cin >> amount;
        input += 1;
        if (amount <= 0) {
            cout << "Invalid operation!" << endl;
            break;
        } else {
            money += amount;
            cout << "Increase: " << amount << endl;
        }
    }
    cout << "Total: " << money << endl;
    return 0;
}