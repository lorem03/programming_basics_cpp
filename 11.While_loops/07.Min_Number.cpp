#include <iostream>
using namespace std;
int main() {
    int n, nums, min = INT32_MAX;
    cin >> n;
    while (n > 0) {
        cin >> nums;
        n -= 1;
        if (nums < min) {
            min = nums;
        }
    }
    cout << min << endl;
    return 0;
}