#include <iostream>
#include <string>
using namespace std;
int main() {
    int sum = 0;
    string input;
    cin >> input; 
    while (input != "Stop") {
        int num = stoi(input);
        sum += num;
        cin >> input; 
    }
    cout << sum << endl;
    return 0;
}