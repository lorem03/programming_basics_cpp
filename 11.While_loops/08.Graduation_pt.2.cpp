#include <iostream>
using namespace std;
int main() {
    cout.setf(ios::fixed);
    cout.precision(2);
    
    string name;
    double grade;
    int level = 1, fail = 0;
    cin >> name;
    while (level <= 12 && fail < 2) {
        double tmpGrade;
        cin >> tmpGrade;
        if (tmpGrade < 4) {
            fail += 1;
        } else {
            grade += tmpGrade;
            level += 1;
        }
    }
    if (fail == 2) {
        cout << name << " has been excluded at " << level << " grade" << endl;
    } else {
        cout << name << " graduated. Average grade: " << grade / 12 << endl;
    }
    return 0;
}