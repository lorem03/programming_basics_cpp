#include <iostream>
using namespace std;
int main() {
    string type;
    cin >> type;
    if (type == "dog") {
        cout << "mammal" << endl;
    } else if (type == "crocodile" || type == "tortoise" || type == "snake") {
        cout << "reptile" << endl;
    } else {
        cout << "unknown" << endl;
    }
    return 0;
}