#include <iostream>
using namespace std;
int main() {
    int days, nights;
    string roomType, grade, intDays;
    double price;

    getline(cin, intDays);
    getline(cin, roomType);
    getline(cin, grade);
    days = stoi(intDays);
    
    nights = days - 1;
    if (roomType == "room for one person") {
        price = nights * 18 ;
    } else if (roomType == "apartment") {
        if (nights < 10) {
            price = (nights * 25) * 0.7;
        } else if (nights >= 10 && nights <= 15) {
            price = (nights * 25) * 0.65;
        } else {
            price = (nights * 25) * 0.5;
        }
    } else {
        if (nights < 10) {
            price = (nights * 35) * 0.9;
        } else if (nights >= 10 && nights <= 15) {
            price = (nights * 35) * 0.85;
        } else {
            price = (nights * 35) * 0.80;
        }
    }
    cout.setf(ios::fixed);
    cout.precision(2);
    if (grade == "positive") {
        cout << price * 1.25 << endl;
    } else {
        cout << price * 0.9 << endl;
    }
    return 0;
}