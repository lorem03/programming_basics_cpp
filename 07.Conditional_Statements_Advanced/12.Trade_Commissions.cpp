#include <iostream>
using namespace std;
int main() {
    string city;
    double money, error, final;
    cin >> city >> money;
    error = 0;
    if (city == "Sofia") {
        if (money >= 0 && money <= 500) {
            final = money * 0.05;
        } else if (money > 500 && money <= 1000) {
            final = money * 0.07;
        } else if (money > 1000 && money <= 10000) {
            final = money * 0.08;
        } else if (money > 10000) {
            final = money * 0.12;
        } else {
            error = 1;
        }
    } else if (city == "Plovdiv") {
        if (money >= 0 && money <= 500) {
            final = money * 0.055;
        } else if (money > 500 && money <= 1000) {
            final = money * 0.08;
        } else if (money > 1000 && money <= 10000) {
            final = money * 0.12;
        } else if (money > 10000) {
            final = money * 0.145;
        } else {
            error = 1;
        }
    } else if (city == "Varna") {
        if (money >= 0 && money <= 500) {
            final = money * 0.045;
        } else if (money > 500 && money <= 1000) {
            final = money * 0.075;
        } else if (money > 1000 && money <= 10000) {
            final = money * 0.1;
        } else if (money > 10000) {
            final = money * 0.13;
        } else {
            error = 1;
        }
    } else {
        error = 1;
    }
    cout.setf(ios::fixed);
    cout.precision(2);
    if (error == 0) {
        cout << final << endl;
    } else {
        cout << "error" << endl;
    }
    
    return 0;
}