#include <iostream>
using namespace std;
int main() {
    double ages;
    char gender;
    cin >> ages >> gender;
    if ( gender == 'm' && ages >= 16 ) {
        cout << "Mr." << endl;
    } else if ( gender == 'm' && ages < 16 ) {
        cout << "Master" << endl;
    } else if ( gender == 'f' && ages >= 16 ) {
        cout << "Ms." << endl;
    } else {
        cout << "Miss" << endl;
    }
    return 0;
}