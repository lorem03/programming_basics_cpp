#include <iostream>
using namespace std;
int main() {
    double changeInput;
    cin >> changeInput;
    int change = changeInput * 100, coins = 0;
    if (change >= 200) {
        coins = change / 200;
        change = change % 200;
    }
    if (change >= 100) {
        change -= 100;
        coins++;
    }
    if (change >= 50) {
        change -= 50;
        coins++;
    }
    if (change >= 20) {
        change -= 20;
        coins++;
    }
    if (change >= 10) {
        change -= 10;
        coins++;
    }
    if (change >= 5) {
        change -= 5;
        coins++;
    }
    if (change >= 2) {
        change -= 2;
        coins++;
    }
    if (change >= 1) {
        change -= 1;
        coins++;
    }
    cout << coins << endl;
    return 0;
}