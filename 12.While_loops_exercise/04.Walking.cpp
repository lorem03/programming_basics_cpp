#include <iostream>
#include <string>
using namespace std;
int main() {
    int stepsTarget = 10000;
    string stepsInput;
    while (stepsTarget > 0) {
        getline(cin, stepsInput);
        if (stepsInput == "Going home") {
            int stepsTemp;
            cin >> stepsTemp, cin.ignore();
            stepsTarget -= stepsTemp;
            break;
        } else {
            int stepStoi = stoi(stepsInput);
            stepsTarget -= stepStoi;
        }
    }
    if (stepsTarget > 0) {
        cout << stepsTarget << " more steps to reach goal." <<endl;
    } else {
        cout << "Goal reached! Good job!" << endl;
    }
    return 0;
}