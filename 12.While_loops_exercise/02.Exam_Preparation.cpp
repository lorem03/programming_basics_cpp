#include <iostream>
#include <string>

using namespace std;
int main() {
    cout.setf(ios::fixed);
    cout.precision(2);
    string lowgradeInput;
    getline(cin, lowgradeInput);
    int lowgrade = stoi(lowgradeInput), problems = 0, lowgradeCounetr = 0;
    bool stop = false;
    double averageScore = 0;
    string lastProblem;
    while (!stop) {
        if (lowgradeCounetr != lowgrade) {
            string problemName;
            getline(cin, problemName);
            if (problemName == "Enough") {
                stop = true;
                break;
            } else {
                lastProblem = problemName;
                string gradeInput;
                getline(cin, gradeInput);
                int grade = stoi(gradeInput);
                averageScore += grade;
                problems++;
                if (grade <= 4) {
                    lowgradeCounetr++;
                }
                }
        } else {
            break;
        }
    }
    if (!stop) {
        cout << "You need a break, " << lowgrade << " poor grades." << endl;
    } else {
        cout << "Average score: " << averageScore / problems << endl;
        cout << "Number of problems: " << problems << endl;
        cout << "Last problem: " << lastProblem << endl;
    }
    return 0;
}