#include <iostream>
#include <string>

using namespace std;
int main() {
    double tripPrice, cash, savedMoney, moneyAction;
    cin >> tripPrice >> savedMoney;
    int actionCounter = 5, days = 0;
    string lastAction;
    bool done = false;
    while (!done) {
        if (actionCounter == 0) {
            done = true;
            break;
        }
        string actionInput;
        cin >> actionInput >> moneyAction;
        if (actionInput == "save") {
            savedMoney += moneyAction;
            days++;
            actionCounter = 5;
            if (tripPrice <= savedMoney) {
                done = true;
                break;
            }
        } else {
            actionCounter--;
            days++;
            savedMoney -= moneyAction;
            if (savedMoney <= 0) {
                savedMoney = 0;
            }
        }
    }
    if (done) {
        if (tripPrice <= savedMoney && actionCounter > 0) {
            cout << "You saved the money for " << days << " days." << endl;
        } else {
            cout << "You can't save the money." << endl;
            cout << days << endl;
        }
    }
    return 0;
}