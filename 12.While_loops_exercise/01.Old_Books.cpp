#include <iostream>
#include <string>
using namespace std;
int main() {
    string fBook;
    getline(cin, fBook);
    int bcount;
    cin >> bcount;
    int counter = -1;
    bool finded = false;
    while (counter < bcount) {
        string nextBook;
        getline(cin, nextBook);
        if (nextBook == fBook) {
            finded = true;
            break;
        } else {
            counter++;
        }

    }
    if (!finded) {
        cout << "The book you search is not here!" << endl;
        cout << "You checked " << counter << " books." << endl;
    } else {
        cout << "You checked " << counter << " books and found it." << endl;
    }
    return 0;
}