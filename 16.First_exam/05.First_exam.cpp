#include <iostream>
#include <string>
using namespace std;
int main() {
    bool flag = false;
    int adults = 0, kids = 0;
    double moneyToys = 0, moneySweaters = 0;
    while (!flag) {
        string input;
        getline(cin, input);
        if (input == "Christmas") {
            flag = true;
            break;
        } else {
            int ages = stoi(input);
            if (ages <= 16) {
                kids++;
                moneyToys += 5;
            } else {
                adults++;
                moneySweaters += 15;
            }
        }
    }
    cout << "Number of adults: " << adults << endl;
    cout << "Number of kids: " << kids << endl;
    cout << "Money for toys: " << moneyToys << endl;
    cout << "Money for sweaters: " << moneySweaters << endl;
    return 0;
}