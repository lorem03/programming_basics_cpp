#include <iostream>
#include <math.h>
using namespace std;
int main() {
    double width, lenght, height, midAstrHeight;
    cin >> width >> lenght >> height >> midAstrHeight;
    double roomSpace, rocketSpace;
    roomSpace = (midAstrHeight + 0.4) * 2 * 2;
    rocketSpace = width * lenght * height;
    int astr = floor(rocketSpace / roomSpace);
    if (astr >= 3 && astr <= 10) {
        cout << "The spacecraft holds " << astr << " astronauts." << endl;
    } else if (astr < 3) {
        cout << "The spacecraft is too small." << endl;
    } else {
        cout << "The spacecraft is too big." << endl;
    }
    
    return 0;
}