#include <iostream>
using namespace std;
int main() {
    string type;
    int quantityOrders, daysOfDec;
    cin >> type >> quantityOrders >> daysOfDec;
    double cakePrice = 0, soufflePrice = 0, baklavaPrice = 0, totalPrice = 0, finalPrice = 0;
    if (daysOfDec <= 15) {
        cakePrice = 24, soufflePrice = 6.66, baklavaPrice = 12.6;
    } else {
        cakePrice = 28.7, soufflePrice = 9.8, baklavaPrice = 16.98;
    }
    if (type == "Cake") {
       totalPrice = cakePrice * quantityOrders; 
    } else if (type == "Souffle") {
        totalPrice = soufflePrice * quantityOrders;
    } else {
        totalPrice = baklavaPrice * quantityOrders;
    }
    if (daysOfDec <= 22) {
        if (totalPrice >= 100 && totalPrice <= 200) {
            finalPrice = totalPrice * 0.85;
        } else if (totalPrice > 200) {
            finalPrice = totalPrice * 0.75;
        } else {
            finalPrice = totalPrice;
        }
        if (daysOfDec <= 15) {
            finalPrice *= 0.9;
        } 
    } else {
        finalPrice = totalPrice;
    }
    cout.setf(ios::fixed);
    cout.precision(2);
    cout << finalPrice << endl;

    return 0;
}