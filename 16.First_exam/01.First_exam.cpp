#include <iostream>
#include <math.h>
using namespace std;
int main() {
    double speed, fuelPerHKm, distance = 768800, time, fuel = 0;
    cin >> speed >> fuelPerHKm;
    time = ceil(distance / speed) + 3;
    fuel = (fuelPerHKm * distance) / 100;
    cout << time << endl;
    cout << fuel << endl;
    return 0;
}