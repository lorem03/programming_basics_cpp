#include <iostream>
#include <math.h>
using namespace std;
int main() {
    int locations;
    cin >> locations;
    for (int i = 1; i <= locations; i++) {
        double target = 0, digg = 0, midDig = 0;
        int days = 0;
        cin >> target >> days;
        for (int d = 1; d <= days; d++) {
            cin >> digg;
            midDig += digg;
        }
        cout.setf(ios::fixed);
        cout.precision(2);
        
        if (midDig / days >= target) {
            cout << "Good job! Average gold per day: " << midDig / days << "." << endl;
        } else {
            cout << "You need " << abs(target - midDig / days)  << " gold." << endl;
        }
    }
    return 0;
}