#include <iostream>
using namespace std;
int main() {
    int cats;
    int foodWeight, grOne = 0, grTwo = 0, grThree = 0; 
    double totalFood = 0;
    cin >> cats;
    for (int i = 1; i <= cats; i++) {
        cin >> foodWeight;
        if (foodWeight >= 100 && foodWeight < 200) {
            grOne++;
            totalFood += foodWeight;
        } else if (foodWeight >= 200 && foodWeight < 300) {
            grTwo++;
            totalFood += foodWeight;
        } else {
            grThree++;
            totalFood += foodWeight;
        }
    }
    cout.setf(ios::fixed);
    cout.precision(2);
    
    cout << "Group 1: " << grOne << " cats." << endl;
    cout << "Group 2: " << grTwo << " cats." << endl;
    cout << "Group 3: " << grThree << " cats." << endl;
    cout << "Price for food per day: " << (totalFood / 1000) * 12.45 << " lv." << endl;
    return 0;
}