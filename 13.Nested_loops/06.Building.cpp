#include <iostream>
using namespace std;
int main() {
    int floor, flat;
    cin >> floor >> flat;
    for (int i = floor; i > 0; i--) {
        for (int a = 0; a < flat; a++) {
            if (i == floor) {
                cout << "L" << i << a << " ";
            } else {
                if (i % 2 == 0) {
                    cout << "O" << i << a << " ";
                } else {
                    cout << "A" << i << a << " ";
                }
            }
        }
        cout << endl;
    }
    return 0;
}