#include <iostream>
#include <string>
using namespace std;
int main() {
    cout.setf(ios::fixed);
    cout.precision(2);
    
    double savedMoney = 0, tripPrice = 0, moneyIn = 0;
    bool flag = false;
    string destination;
    while (!flag) {
        getline(cin, destination);
        if (destination == "End") {
            flag = true;
            break;
        }
        cin >> tripPrice, cin.ignore();
        while (savedMoney < tripPrice) {
            cin >> moneyIn, cin.ignore();
            savedMoney += moneyIn;
            if (savedMoney >= tripPrice) {
                savedMoney = 0;
                cout << "Going to " << destination << "!" << endl;
                break;
            }
        }
    }
    return 0;
}