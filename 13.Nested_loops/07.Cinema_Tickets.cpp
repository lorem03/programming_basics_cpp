#include <iostream>
#include <locale>
using namespace std;
int main() {
    cout.setf(ios::fixed);
    cout.precision(2);
    string movie;
    bool finish = false;
    int seats = 0, tickets = 0, totalStudent = 0, totalStandard = 0, totalKid = 0;
    while (!finish) {
        getline(cin, movie);
        int studentTickets = 0, standardTickets = 0, kidTickets = 0;
        if (movie != "Finish") {
            cin >> seats, cin.ignore();
            for (int i = 1; i <= seats; i++) {
                string type;
                getline(cin, type);
                if (type == "student") {
                    tickets++;
                    totalStudent++;
                    studentTickets++;
                } else if (type == "standard") {
                    tickets++;
                    totalStandard++;
                    standardTickets++;
                } else if (type == "kid") {
                    tickets++;
                    totalKid++;
                    kidTickets++;
                } else {
                    break;
                }
            }
            cout << movie << " - " << (studentTickets + standardTickets + kidTickets) / (seats * 1.0) * 100 << "% full." << endl;
        } else {
            break;
        }
    }
    cout << "Total tickets: " << tickets << endl;
    cout << totalStudent / (tickets * 1.0) * 100 << "% student tickets." << endl;
    cout << totalStandard / (tickets * 1.0) * 100 << "% standard tickets." << endl;
    cout << totalKid / (tickets * 1.0) * 100 << "% kids tickets." << endl;
    return 0;
}