#include <iostream>
using namespace std;
int main() {
  int f, l, m, counter = 0;
  cin >> f >> l >> m;
  bool done = false;
  for (int i = f; i <= l; i++) {
    for (int a = f; a <= l; a++) {
      counter++;
      if (i + a == m) {
        cout << "Combination N:" << counter << " (" << i << " + " << a << " = " << m << ")" << endl;
        done = true;
      }
    }
    if (done) {
      break;
    }
  }
  if (!done) {
      cout << counter << " combinations - neither equals " << m << endl;
  }
  return 0;
}