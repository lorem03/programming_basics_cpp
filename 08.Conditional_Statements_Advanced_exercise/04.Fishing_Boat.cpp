#include <iostream>
using namespace std;
int main() {
    int fishers, budget, rent;
    double price;
    string season;
    cin >> budget >> season >> fishers;
    if (season == "Spring") {
        rent = 3000;
    } else if (season == "Winter") {
        rent = 2600;
    } else {
        rent = 4200;
    }
    if (fishers <= 6) {
        price = rent * 0.9;
    } else if (fishers >= 7 && fishers <= 11) {
        price = rent * 0.85;
    } else {
        price = rent * 0.75;
    }
    if (fishers % 2 == 0 && season != "Autumn") {
        price *= 0.95;
    }
    cout.setf(ios::fixed);
    cout.precision(2);
    if (budget >= price) {
        cout << "Yes! You have " << budget - price << " leva left." << endl;
    } else {
        cout << "Not enough money! You need " << price - budget << " leva." << endl;
    }
}