#include <iostream>
using namespace std;
int main() {
    string type;
    int quantity, budget;
    double price;
    cin >> type >> quantity >> budget;
    if (type == "Roses") {
        price = (quantity > 80) ? 5 * 0.9 : 5;
    } else if (type == "Dahlias") {
        price = (quantity > 90) ? 3.8 * 0.85 : 3.8;
    } else if (type == "Tulips") {
        price = (quantity > 80) ? 2.8 * 0.85 : 2.8;
    } else if (type == "Narcissus") {
        price = (quantity < 120) ? 3 * 1.15 : 3;
    } else if (type == "Gladiolus") {
        price = (quantity < 80) ? 2.5 * 1.2 : 2.5;
    }
    cout.setf(ios::fixed);
    cout.precision(2);
    if (budget >= price * quantity) {
        cout << "Hey, you have a great garden with " << quantity << " " << type <<" and " << budget - price * quantity << " leva left." << endl;
    } else {
        cout << "Not enough money, you need " << price * quantity - budget << " leva more." << endl;
    }
    return 0;
}