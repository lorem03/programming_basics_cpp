#include <iostream>
using namespace std;
int main() {
    string type;
    int x,y;
    double price;
    cin >> type >> x >> y;
    if (type == "Premiere") {
        price = 12;
    } else if (type == "Normal") {
        price = 7.5;
    } else {
        price = 5;
    }
    cout.setf(ios::fixed);
    cout.precision(2);
    cout << price * x * y << " leva" << endl;

    return 0;
}