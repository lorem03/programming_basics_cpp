#include <iostream>
using namespace std;
int main() {
    int n1, n2, nan, sum;
    double sumd;
    string oprt, type;
    nan = 1;
    type = "none";
    cin >> n1 >> n2 >> oprt;
    if (oprt == "+") {
        sum = n1 + n2;
        type = (sum % 2 == 0) ? "even" : "odd";
    } else if (oprt == "-") {
        sum = n1 - n2;
        type = (sum % 2 == 0) ? "even" : "odd";
    } else if (oprt == "*") {
        sum = n1 * n2;
        type = (sum % 2 == 0) ? "even" : "odd";
    } else if (oprt == "%") {
        if (n2 == 0) {
            nan = 0;
        } else {
            sum = n1 % n2;
        }
    } else {
        cout.setf(ios::fixed);
        cout.precision(2);
        nan = (n2 == 0) ? 0 : 1;
        sumd = ((n1 * 1.0) / n2);
    }
    if (nan != 0 && type != "none") {
        cout << n1 << " " << oprt << " " << n2 << " = " << sum << " - " << type << endl;
    } else if (nan != 0 && oprt == "/") {
        cout << n1 << " " << oprt << " " << n2 << " = " << sumd << endl;
    } else if (nan != 0) {
        cout << n1 << " " << oprt << " " << n2 << " = " << sum << endl;
    } else {
        cout << "Cannot divide " << n1 << " by zero" << endl;
    }
    return 0;
}