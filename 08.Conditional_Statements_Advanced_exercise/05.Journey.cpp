#include <iostream>
using namespace std;
int main() {
    double budget, price;
    string season, destination, type;
    cin >> budget >> season;
    if (budget <= 100) {
        destination = "Bulgaria";
        price = (season == "summer") ? budget * 0.7 : budget * 0.3;
        type = (season == "summer") ? "Camp" : "Hotel";
    } else if (budget > 100 && budget <= 1000) {
        destination = "Balkans";
        price = (season == "summer") ? budget * 0.6 : budget * 0.2;
        type = (season == "summer") ? "Camp" : "Hotel";
    } else {
        destination = "Europe";
        price = budget * 0.1;
        type = "Hotel";
    }
    cout << "Somewhere in " << destination  << endl;
    cout.setf(ios::fixed);
    cout.precision(2);
    cout << type << " - " << budget - price << endl;
    return 0;
}