#include <iostream>
using namespace std;
int main() {
    int inH, inM, exH, exM, examTime, arrivTime;
    cin >> exH >> exM >> inH >> inM;
    examTime = exH * 60 + exM;
    arrivTime = inH * 60 + inM;
    if ( examTime == arrivTime) {
        cout << "On time" << endl;
    } else if (arrivTime > examTime) {
        cout << "Late" << endl;
        if (arrivTime - examTime < 60) {
            cout << arrivTime - examTime << " minutes after the start" << endl;
        } else {
            if ((arrivTime - examTime) % 60 < 9) {
                cout << (arrivTime - examTime) / 60 << ":0" << (arrivTime - examTime) % 60 << " hours after the start" << endl;
            } else {
                cout << (arrivTime - examTime) / 60 << ":" << (arrivTime - examTime) % 60 << " hours after the start" <<endl;
            }
        }
    } else {
        if (examTime - arrivTime <= 30) {
        cout << "On time" << endl;
        cout << examTime - arrivTime << " minutes before the start" << endl;
        } else if (examTime - arrivTime > 30 && examTime - arrivTime < 60) {
            cout << "Early" << endl;
            cout << examTime - arrivTime << " minutes before the start" << endl;
        } else {
            cout << "Early" << endl;
            if ((examTime - arrivTime) % 60 < 9) {
                cout << (examTime - arrivTime) / 60 << ":0" << (examTime - arrivTime) % 60 << " hours before the start" << endl;
            } else {
                cout << (examTime - arrivTime) / 60 << ":" << (examTime - arrivTime) % 60 << " hours before the start" <<endl;
            }
        }
    }
    return 0;
}