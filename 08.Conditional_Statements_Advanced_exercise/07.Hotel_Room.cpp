#include <iostream>
using namespace std;
int main() {
    string month;
    int nights;
    double priceStudio, priceApartment;
    cin >> month >> nights;
    if (month == "May" || month == "October") {
        priceStudio = 50;
        priceApartment = 65;
        if (nights > 7 && nights <= 14) {
            priceStudio *= 0.95; 
        } else if (nights > 14) {
            priceStudio *= 0.7;
        }
    } else if (month == "June" || month == "September") {
        priceStudio = 75.2;
        priceApartment = 68.7;
        if (nights > 14) {
            priceStudio *= 0.8;
        }
    } else {
        priceStudio = 76;
        priceApartment = 77;
    }
    if (nights > 14) {
        priceApartment *= 0.9;
    }
    cout.setf(ios::fixed);
    cout.precision(2);
    cout << "Apartment: " << priceApartment * nights << " lv." << endl;
    cout << "Studio: " << priceStudio * nights << " lv." << endl;
    return 0;
}