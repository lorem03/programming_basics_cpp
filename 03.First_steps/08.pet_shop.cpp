#include "iostream"
using namespace std;
int main() {
    int dogs;
    int pets;
    cin >> dogs;
    cin >> pets;
    double total = (dogs * 2.5) + (pets * 4);
    cout.setf(ios::fixed);
    cout.precision(2);
    cout << total << " lv." << endl;
    return 0;
}