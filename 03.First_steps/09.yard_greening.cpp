#include "iostream"
using namespace std;
int main() {
    double area;
    cin >> area;
    double totalPrice = area * 7.61;
    double finalPrice = totalPrice * 0.82;
    double discount = totalPrice - finalPrice;
    cout.setf(ios::fixed);
    cout.precision(2);
    cout << "The final price is: " << finalPrice << " lv." << endl;
    cout << "The discount is: " << discount << " lv." << endl;
    return 0;
}