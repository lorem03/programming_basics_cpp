#include <iostream>
using namespace std;
int main() {
    int minInput, secInput, minToSec, minOutput, secOutput, final, fmin, fsec;
    cin >> minInput >> secInput;
    minToSec = minInput * 60;
    final = minToSec + secInput + 15;
    minOutput = final / 60;
    secOutput = final % 60;
    if (final >= 1440 && final <= 1449) {
        cout << "0:0" << secOutput << endl;
    } else if  (final >= 1440) {
        cout << "0:" << secOutput << endl;
    } else if (secOutput <= 9) {
        cout << minOutput << ":0" << secOutput << endl;
    } else {
        cout << minOutput << ":" << secOutput << endl;
    }
    return 0;
}