#include <iostream>
using namespace std;
int main() {
    double budget, clothesPrice, cost;
    int people;
    cin >> budget >> people >> clothesPrice;
    clothesPrice = people * clothesPrice;
    if (people > 150) {
        cost = clothesPrice * 0.9 + budget * 0.1;
    } else {
        cost = clothesPrice + budget * 0.1;
    }
    cout.setf(ios::fixed);
    cout.precision(2);
    
    if (cost > budget) {
        cout << "Not enough money!" << endl;
        cout << "Wingard needs " << cost - budget << " leva more." << endl;
    } else {
        cout << "Action!" << endl;
        cout << "Wingard starts filming with " << budget - cost << " leva left." << endl;
    }

    return 0;
}