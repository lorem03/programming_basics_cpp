#include <iostream>
#include <string>
using namespace std;
int main() {
    double distance, converted;
    string input, output;
    cin >> distance >> input >> output;
    if (input == "mm" && output == "cm") {
        converted = distance * 0.1;
    }
    if (input == "mm" && output == "m") {
        converted = distance * 0.001;
    }
    if (input == "cm" && output == "mm") {
        converted = distance * 10;
    }
    if (input == "cm" && output == "m") {
        converted = distance * 0.01;
    }
    if (input == "m" && output == "mm") {
        converted = distance * 1000;
    }
    if (input == "m" && output == "cm") {
        converted = distance * 100;
    }
    cout.setf(ios::fixed);
    cout.precision(3);
    cout << converted << endl;
    return 0;
}