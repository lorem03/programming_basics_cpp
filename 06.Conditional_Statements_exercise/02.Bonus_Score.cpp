#include <iostream>
using namespace std;
int main() {
    int number;
    double bonus;
    cin >> number;
    if (number <= 100) {
        bonus = 5;
    } else if (number > 100 && number <= 1000) {
        bonus = number * 0.2;
    } else {
        bonus = number * 0.1;
    }
    if (number % 2 == 0) {
        bonus += 1;
    } 
    if (number % 10 == 5) {
        bonus += 2;
    } 
    cout << bonus << endl;
    cout << bonus + number << endl;
    return 0;
}