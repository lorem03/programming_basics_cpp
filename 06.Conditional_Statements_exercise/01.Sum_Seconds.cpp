#include <iostream>
using namespace std;
int main() {
    int first, second, third, total, minutes, seconds;
    cin >> first >> second >> third;
    total = first + second + third;
    minutes = total / 60;
    seconds = total % 60;
    if (seconds < 10) {
        cout << minutes << ":0" << seconds << endl;
    } else {
        cout << minutes << ":" << seconds << endl;
    }
    return 0;
}