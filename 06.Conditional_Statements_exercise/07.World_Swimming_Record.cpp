#include <iostream>
#include <math.h>
using namespace std;
int main() {
    double record, recordTime, distance, ivanTarget, ivanTime, ivanSlowing, ivanTimePerMeter;
    cin >> recordTime >> distance >> ivanTimePerMeter;
    record = recordTime / distance;
    ivanTarget = ivanTimePerMeter * distance;
    ivanSlowing = floor(distance / 15) * 12.5;
    ivanTime = ivanTarget + ivanSlowing;
    cout.setf(ios::fixed);
    cout.precision(2);
    if (ivanTime < recordTime) {
        cout <<  "Yes, he succeeded! The new world record is " << ivanTime << " seconds." << endl;
    } else {
        cout << "No, he failed! He was " << ivanTime - recordTime << " seconds slower." << endl;
    }
    return 0;
}