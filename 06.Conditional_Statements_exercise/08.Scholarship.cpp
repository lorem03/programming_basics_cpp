#include <iostream>
#include <math.h>
using namespace std;
int main() {
    double earnings, grade, minWage, social, greater;
    social = 0;
    greater = 0;
    cin >> earnings >> grade >> minWage;
    if (earnings < minWage && grade > 4.5) {
        social = minWage * 0.35;
    } 
    if (grade >= 5.5) {
        greater = grade * 25;
    }
    if (social > greater) {
        cout << "You get a Social scholarship " << floor(social) <<" BGN" << endl;
    } else if (greater >= social && greater != 0) {
        cout << "You get a scholarship for excellent results " << floor(greater) << " BGN" << endl;
    } else {
        cout << "You cannot get a scholarship!" << endl;
    }
    
    return 0;
}