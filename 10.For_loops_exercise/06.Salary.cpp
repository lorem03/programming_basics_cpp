#include <iostream>
using namespace std;
int main() {
    int tabs;
    int salary;
    cin >> tabs >> salary;
    for (int i = 1; i <= tabs; i++) {
        string tab;
        cin >> tab;
        if (tab == "Facebook") {
            salary -= 150;
        } else if (tab == "Instagram") {
            salary -= 100;
        } else if (tab == "Reddit") {
            salary -= 50;
        }
        if (salary <= 0) {
            break;
        }
    }
    if (salary <= 0) {
        cout << "You have lost your salary." << endl;
    } else {
        cout << salary << endl;
    }
    return 0;
}