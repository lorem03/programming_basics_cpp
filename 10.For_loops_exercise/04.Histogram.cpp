#include <iostream>
using namespace std;
int main() {
    int num, temp; 
    double p1 = 0, p2 = 0, p3 = 0, p4 = 0, p5 = 0;
    cin >> num;
    for (int i = 1; i <= num; i++) {
        cin >> temp;
        if (temp < 200) {
            p1 += 1;
        } else if (temp >= 200 && temp < 400) {
            p2 += 1;
        } else if (temp >= 400 && temp < 600) {
            p3 += 1;
        } else if (temp >= 600 && temp < 800) {
            p4 += 1;
        } else {
            p5 += 1;
        }
    }
    cout.setf(ios::fixed);
    cout.precision(2);
    cout << p1 / num * 100 << "%" << endl;
    cout << p2 / num * 100 << "%" << endl;
    cout << p3 / num * 100 << "%" << endl;
    cout << p4 / num * 100 << "%" << endl;
    cout << p5 / num * 100 << "%" << endl;
    return 0;
}