#include <iostream>
using namespace std;
int main() {
    int num, temp;
    double p1 = 0, p2 = 0, p3 = 0;
    cin >> num;
    for (int i = 1; i <= num; i++) {
        cin >> temp;
        if (temp % 2 == 0) {
            p1 += 1;
        } 
        if (temp % 3 == 0) {
            p2 += 1;
        }
        if (temp % 4 == 0) {
            p3 += 1;
        }
    }
    cout.setf(ios::fixed);
    cout.precision(2);
    
    cout << p1 / num * 100 << "%" << endl;
    cout << p2 / num * 100 << "%" << endl;
    cout << p3 / num * 100 << "%" << endl;
    return 0;
}