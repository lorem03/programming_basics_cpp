#include <cstdint>
#include <iostream>
using namespace std;
int main() {
    cout.setf(ios::fixed);
    cout.precision(2);
    
    int n;
    double evenMin = INT32_MAX, evenMax = INT32_MIN, evenSum = 0;
    double oddMin = INT32_MAX, oddMax = INT32_MIN, oddSum = 0;
    cin >> n;
    for (int i = 1; i <= n; i++) {
        double temp;
        cin >> temp;
        if (i % 2 != 0) {
            oddSum += temp;
            if (temp > oddMax) {
                oddMax = temp;
            }
            if (temp < oddMin) {
                oddMin = temp;
            }
        } else {    
            evenSum += temp;
            if (temp > evenMax) {
                evenMax = temp;
            }
            if (temp < evenMin) {
                evenMin = temp;
            }
        }
    }
    cout << "OddSum=" << oddSum << "," << endl;
    if (oddMin == INT32_MAX) {
        cout << "OddMin=No" << "," << endl;
    } else {
        cout << "OddMin=" << oddMin << "," << endl;
    }
    if (oddMax == INT32_MIN) {
        cout << "OddMax=No" <<  "," << endl;
    } else {
        cout << "OddMax=" << oddMax <<  "," << endl;
    }
    cout << "EvenSum=" << evenSum <<  "," << endl;
    if (evenMin == INT32_MAX) {
        cout << "EvenMin=No" <<  "," << endl;
    } else {
        cout << "EvenMin=" << evenMin << "," << endl;
    }
    if (evenMax == INT32_MIN) {
        cout << "EvenMax=No" << endl;
    } else {
        cout << "EvenMax=" << evenMax  << endl;
    }


    
    return 0;
}