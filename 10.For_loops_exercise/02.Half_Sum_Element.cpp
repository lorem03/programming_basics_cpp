#include <cstdint>
#include <iostream>
using namespace std;
int main() {
    int n, biggest = INT32_MIN, other = 0; 
    cin >> n;
    for (int i = 1; i <= n; i++) {
        int tempInput;
        cin >> tempInput;
        if (tempInput > biggest) {
            if (biggest != INT32_MIN) {
                other += biggest;
            }
            biggest = tempInput;
        } else {
            other += tempInput;
        } 
    }
    if (biggest == other) {
        cout << "Yes" << endl;
        cout << "Sum = " << biggest << endl;
    } else {
        cout << "No" << endl;
        cout << "Diff = " << abs(other - biggest) << endl;

    }
    return 0;
}