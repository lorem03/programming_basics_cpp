#include <iostream>
using namespace std;
int main() {
    cout.setf(ios::fixed);
    cout.precision(2);
    
    int ages, toys = 0, toyPrice;
    double washingMach, savedMoney = 0;
    cin >> ages >> washingMach >> toyPrice;
    for (int i = 1; i <= ages; i++) {
        if (i % 2 != 0) {
            toys += 1;
        }
    }
    int evenBD = ages - toys;
    for (int i = 1; i <= evenBD; i++) {
        savedMoney += i * 10;
    }
    double finalMoney = (savedMoney - evenBD) + toys * toyPrice;
    if (finalMoney >= washingMach) {
        cout << "Yes! " << finalMoney - washingMach << endl;
    } else {
        cout << "No! " << washingMach - finalMoney << endl;
    }
    return 0;
}