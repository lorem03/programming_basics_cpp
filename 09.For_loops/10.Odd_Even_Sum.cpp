#include <iostream>
using namespace std;
int main() {
    int num, even = 0, odd = 0;
    cin >> num;
    for (int i = 1; i < num + 1; i++) {
        int tmpNum;
        cin >> tmpNum;
        i % 2 == 0 ? even += tmpNum : odd += tmpNum;
    }
    if (even == odd) {
        cout << "Yes" << endl;
        cout << "Sum = " << even << endl;
    } else {
        cout << "No" << endl;
        cout << "Diff = " << abs(even - odd) << endl;
    }
    return 0;
}