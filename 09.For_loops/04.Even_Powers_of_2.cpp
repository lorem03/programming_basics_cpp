#include <iostream>
#include <math.h>
using namespace std;
int main() {
    int num;
    cin >> num;
    for (int i = 0; i <= num; i += 2) {
        int value = pow(2, i);
        cout << value << endl;
    }
    return 0;
}