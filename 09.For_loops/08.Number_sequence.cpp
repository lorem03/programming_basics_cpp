#include <cstdint>
#include <iostream>
using namespace std;
int main() {
    int maxNum = INT32_MIN, minNum = INT32_MAX, n;
    cin >> n;
    for (int i = 0; i < n; i++) {
    int tempNum;
    cin >> tempNum;
    if (tempNum > maxNum) {
        maxNum = tempNum;
    }
    if (tempNum < minNum) {
        minNum = tempNum;
    }
    }
    cout << "Max number: " << maxNum << endl;
    cout << "Min number: " << minNum << endl;
    return 0;
}