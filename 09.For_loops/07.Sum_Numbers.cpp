#include <iostream>
using namespace std;
int main() {
    int input;
    cin >> input;
    int sum = 0;
    for (int i = 0; i < input; i++) {
        int tempSum = 0;
        cin >> tempSum;
        sum += tempSum;
    }
    cout << sum << endl;
    return 0;
}