#include <iostream>
#include <string>
using namespace std;
int main() {
    string txt;
    int sum = 0;
    getline(cin, txt);
    for (int i = 0; i < txt.size(); i++ ) {
        if (txt[i] == 'a') {
            sum++;
        } else if (txt[i] == 'e') {
            sum += 2;
        } else if (txt[i] == 'i') {
            sum += 3;
        } else if (txt[i] == 'o') {
            sum += 4;
        } else if (txt[i] == 'u') {
            sum += 5;
        }
    }
    cout << sum << endl;
    return 0;
}