#include <iostream>
#include <math.h>
#include <iomanip>
// #include <algorithm>
using namespace std;
int main() {
    int leftSum = 0, rightSum = 0;
    int count;
    cin >> count;
    for (int i = 1; i < count * 2 + 1; i++) {
        int tempNum = 0;
        cin >> tempNum;
        i <= count ? leftSum += tempNum : rightSum += tempNum;
    }
    
    if (leftSum == rightSum) {
        cout << " Yes, sum = " << leftSum << endl;
    } else {
        cout << "No, diff = " << abs(leftSum - rightSum) << endl;
    }
    return 0;
}