#include <iostream>
using namespace std;
int main() {
    int num1, num2, bigger;
    cin >> num1 >> num2;
    bigger = (num1 > num2)? num1 : num2;
    cout << bigger << endl;
    return 0;
}