#include <iostream>
using namespace std;
int main() {
    double puzzlePrice, dollPrice, teddyBearPrice, minionPrice, truckPrice, totalPrice, tripPrice, finalPrice;
    int puzzle, doll, teddy, minion, truck, totalToys; 
    cin >> tripPrice >> puzzle >> doll >> teddy >> minion >> truck;
    puzzlePrice = puzzle * 2.6;
    dollPrice = doll * 3;
    teddyBearPrice = teddy * 4.1;
    minionPrice = minion * 8.2;
    truckPrice = truck * 2;
    totalToys = puzzle + doll + teddy + minion + truck;
    totalPrice = puzzlePrice + dollPrice + teddyBearPrice + minionPrice + truckPrice;
    finalPrice = (totalToys >= 50) ? totalPrice * 0.75 : totalPrice;
    finalPrice *= 0.9;
    if (finalPrice >= tripPrice) {
        cout.setf(ios::fixed);
        cout.precision(2);
        cout << "Yes! " << finalPrice - tripPrice << " lv left." << endl;
    } else {
        cout.setf(ios::fixed);
        cout.precision(2);
        cout << "Not enough money! " << tripPrice - finalPrice << " lv needed." << endl;
    }
    return 0;
}