#include <iostream>
#include <string>
using namespace std;
int main() {
    string figure;
    double a, b, output;
    cin >> figure;
    if (figure == "square") {
        cin >> a;
        output = a * a;
    } else if (figure == "rectangle") {
        cin >> a >> b;
        output = a * b;
    } else if (figure == "circle") {
        double pi = 3.14159265359;
        cin >> a;
        output = a * a * pi;
    } else {
        cin >> a >> b;
        output = (a * b) / 2;
    }
    cout.setf(ios::fixed);
    cout.precision(3);
    cout << output << endl;

    return 0;
}