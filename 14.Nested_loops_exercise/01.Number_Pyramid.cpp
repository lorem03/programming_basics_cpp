#include <iostream>
using namespace std;
int main() {
    int n = 0, counter = 1;
    cin >> n;
    bool flag = false;
    for (int rows = 1; rows <= n; rows++) {
        for (int cols = 1; cols <= rows; cols++) {
            if (counter > n) {
                flag = true;
                break;
            }
            cout << counter << " ";
            counter++;
        }
        if (flag) {
            break;
        }
        cout << endl;
    }
    return 0;
}