#include <iostream>
using namespace std;
int main() {
    cout.setf(ios::fixed);
    cout.precision(2);
    int problems, problemsCounter = 0;
    cin >> problems, cin.ignore();
    bool flag = false;
    double total = 0;
    while (!flag) {
        string exam;
        getline(cin, exam);
        if (exam == "Finish") {
            flag = true;
            break;
        } else {
            double examGrade = 0;
            for (int i = 1; i <= problems; i++) {
                double grade;
                cin >> grade, cin.ignore();
                examGrade += grade;
                total += grade;
            }
            cout << exam << " - " << examGrade / problems << "." << endl;
            problemsCounter += problems;
        }
    }
    cout << "Student's final assessment is " << total / problemsCounter << "." << endl;
    return 0;
}