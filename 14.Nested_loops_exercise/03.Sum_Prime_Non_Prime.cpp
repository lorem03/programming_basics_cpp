#include <iostream>
using namespace std;
int main() {
    int prime = 0, nonPrime = 0;
    bool flag = false;
    while (!flag) {
        string temp;
        cin >> temp;
        if (temp == "stop") {
            flag = true;
            break;
        } else {
            int num = stoi(temp);
            if (num < 0) {
                cout << "Number is negative." << endl;
            } else {
                int dividedCounter = 0;
                for (int i = 1; i <= num; i++) {
                    if (num % i == 0) {
                        dividedCounter++;
                    }
                }
                dividedCounter == 2 ? prime += num : nonPrime += num;
            }
        }
    
    }
    cout << "Sum of all prime numbers is: " << prime << endl;
    cout << "Sum of all non prime numbers is: " << nonPrime << endl;
    return 0;
}