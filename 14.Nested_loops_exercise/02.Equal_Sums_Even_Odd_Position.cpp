#include <iostream>
#include <string>
#include <stdlib.h>
using namespace std;
int main() {
    int num1, num2;
    cin >> num1 >> num2;
    for (int i = num1; i <= num2;i++) {
        int firstDigit = i / 100000;
        int secondDigit = i / 10000 % 10;
        int thirdDigit = i / 1000 % 10;
        int fourthDigit = i / 100 % 10;
        int fifthDigit = i / 10 % 10;
        int sixthDigit = i % 10;
        int even = secondDigit + fourthDigit + sixthDigit;
        int odd = firstDigit + thirdDigit + fifthDigit;
        if (even == odd) {
            cout << i << " ";
        }
    }
    cout << endl;
    return 0;
}